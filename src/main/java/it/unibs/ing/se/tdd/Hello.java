package it.unibs.ing.se.tdd;

public class Hello {
    private final String phrase;

    public Hello() {
        this("Hello");
    }

    public Hello(String phrase) {
        this.phrase = phrase;
    }

    public String sayHello() {
        return sayHelloTo("World");
    }

    public String sayHelloTo(String to) {
        return String.format("%s, %s!", phrase, to);
    }
}
