package it.unibs.ing.se.tdd;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class HelloTest {
    @Test
    public void sayHelloWorld() {
        var hello = new Hello();
        var result = hello.sayHello();
        assertThat(result, is(equalTo("Hello, World!")));
    }

    @Test
    public void sayHelloToSomeone() {
        var hello = new Hello();
        var result = hello.sayHelloTo("Pietro");
        assertThat(result, is(equalTo("Hello, Pietro!")));
    }

    @Test
    public void canUseCustomPhrase() {
        var hello = new Hello("Hola");
        var result = hello.sayHelloTo("Pietro");
        assertThat(result, is(equalTo("Hola, Pietro!")));
    }
}
